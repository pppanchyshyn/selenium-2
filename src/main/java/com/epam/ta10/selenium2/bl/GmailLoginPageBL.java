package com.epam.ta10.selenium2.bl;

import com.epam.ta10.selenium2.po.GmailLoginPage;

public class GmailLoginPageBL extends AbstractBL{

  private GmailLoginPage gmailLoginPage;

  public GmailLoginPageBL() {
    gmailLoginPage = new GmailLoginPage();
  }

  public void enterLogin(String login) {
    logger.info("Clearing the form...");
    gmailLoginPage.getLoginForm().clear();
    logger.info("Entering user login...");
    gmailLoginPage.getLoginForm().sendKeys(login);
    logger.info("Pressing submit button..");
    gmailLoginPage.getButtonNext().click();
  }
}
