package com.epam.ta10.selenium2.bl;

import com.epam.ta10.selenium2.po.GmailPasswordPage;

public class GmailPasswordPageBL extends AbstractBL {

  private GmailPasswordPage gmailPasswordPage;

  public GmailPasswordPageBL() {
    gmailPasswordPage = new GmailPasswordPage();
  }

  public void enterPassword(String password) {
    logger.info("Clearing the form...");
    gmailPasswordPage.getPasswordForm().clear();
    logger.info("Entering user password...");
    gmailPasswordPage.getPasswordForm().sendKeys(password);
    logger.info("Pressing submit button..");
    gmailPasswordPage.getButtonNext().click();
  }
}
