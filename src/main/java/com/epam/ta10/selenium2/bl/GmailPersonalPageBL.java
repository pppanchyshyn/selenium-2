package com.epam.ta10.selenium2.bl;

import com.epam.ta10.selenium2.po.GmailPersonalPage;
import com.epam.ta10.selenium2.utils.Message;
import com.epam.ta10.selenium2.utils.WebDriverUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailPersonalPageBL extends AbstractBL {

  private GmailPersonalPage gmailPersonalPage;
  private final static int TIMEOUT = 50;

  public GmailPersonalPageBL() {
    gmailPersonalPage = new GmailPersonalPage();
  }

  public void moveToSentMessages() {
    logger.info("Moving to sent messages...");
    clickOn(gmailPersonalPage.getSentMessagesButton());
  }

  public void sendMessage(Message message) {
    logger.info("Pressing button to create new message...");
    gmailPersonalPage.getButtonCreateMessage().click();
    logger.info("Entering message recipient...");
    gmailPersonalPage.getMessageRecipient().sendKeys(message.getRecipient());
    logger.info("Entering message theme...");
    gmailPersonalPage.getMessageTheme().sendKeys(message.getTheme());
    logger.info("Entering message text...");
    gmailPersonalPage.getMessageText().sendKeys(message.getText());
    logger.info("Sending message...");
    gmailPersonalPage.getButtonSend().click();
  }

  public int getSentMessagesCount() {
    logger.info("Getting number of sent messages...");
    int result;
    try {
      result = Integer.valueOf(gmailPersonalPage.getSentMessagesCount().getText());
    } catch (NoSuchElementException e) {
      result = 0;
    }
    return result;
  }

  private void clickOn(WebElement element) {
    new WebDriverWait(WebDriverUtils.getDriver(), TIMEOUT)
        .ignoring(StaleElementReferenceException.class).until(
        ExpectedConditions.elementToBeClickable(element));
    element.click();
  }
}
