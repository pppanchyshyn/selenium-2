package com.epam.ta10.selenium2.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage {

  @FindBy(id = "identifierId")
  private WebElement loginForm;

  @FindBy(xpath = "//div[@id='identifierNext']//child::span[text()]")
  private WebElement buttonNext;

  public WebElement getLoginForm() {
    return loginForm;
  }

  public WebElement getButtonNext() {
    return buttonNext;
  }
}
