package com.epam.ta10.selenium2.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPasswordPage extends AbstractPage {

  @FindBy(name = "password")
  private WebElement passwordForm;

  @FindBy(xpath = "//div[@id='passwordNext']//child::span[text()]")
  private WebElement buttonNext;

  public WebElement getPasswordForm() {
    return passwordForm;
  }

  public WebElement getButtonNext() {
    return buttonNext;
  }
}
