package com.epam.ta10.selenium2.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPersonalPage extends AbstractPage {

  @FindBy(css = "div.z0 > div:first-child")
  private WebElement ButtonCreateMessage;

  @FindBy(name = "to")
  private WebElement messageRecipient;

  @FindBy(name = "subjectbox")
  private WebElement messageTheme;

  @FindBy(xpath = "//div[@role='textbox']")
  private WebElement messageText;

  @FindBy(xpath = "//div[contains(@data-tooltip, 'Enter')]")
  private WebElement buttonSend;

  @FindBy(xpath = "//a[contains(@href, 'sent')]")
  private WebElement sentMessagesButton;

  @FindBy(xpath = "//span[@class='Dj']/span[@class='ts' and (//a[contains(@href, 'sent') and @tabindex='0']) ]")
  private WebElement sentMessagesCount;

  public WebElement getButtonCreateMessage() {
    return ButtonCreateMessage;
  }

  public WebElement getMessageRecipient() {
    return messageRecipient;
  }

  public WebElement getMessageTheme() {
    return messageTheme;
  }

  public WebElement getMessageText() {
    return messageText;
  }

  public WebElement getButtonSend() {
    return buttonSend;
  }

  public WebElement getSentMessagesButton() {
    return sentMessagesButton;
  }

  public WebElement getSentMessagesCount() {
    return sentMessagesCount;
  }
}
