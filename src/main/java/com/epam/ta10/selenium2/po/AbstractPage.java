package com.epam.ta10.selenium2.po;

import com.epam.ta10.selenium2.utils.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractPage {

  private WebDriver driver;

  AbstractPage() {
    this.driver = WebDriverUtils.getDriver();
    PageFactory.initElements(driver, this);
  }
}
