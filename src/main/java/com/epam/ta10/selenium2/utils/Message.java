package com.epam.ta10.selenium2.utils;

public class Message {
private String recipient;
private String theme;
private String text;

  public Message(String recipient, String theme, String text) {
    this.recipient = recipient;
    this.theme = theme;
    this.text = text;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
