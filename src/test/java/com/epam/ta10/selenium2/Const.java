package com.epam.ta10.selenium2;

import com.epam.ta10.selenium2.utils.Message;

public class Const {

  private final static String MESSAGE_RECIPIENT = "pppanchyshyn@gmail.com";
  private final static String MESSAGE_THEME = "Hello";
  private final static String MESSAGE_TEXT = "This is test message";
  public final static Message MESSAGE = new Message(MESSAGE_RECIPIENT, MESSAGE_THEME, MESSAGE_TEXT);

  private Const() {
  }
}
