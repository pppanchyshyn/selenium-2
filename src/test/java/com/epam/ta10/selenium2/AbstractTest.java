package com.epam.ta10.selenium2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

abstract class AbstractTest {

  static Properties properties = new Properties();

  AbstractTest() {
    loadProperties();
  }

  private void loadProperties() {
    try {
      properties.load(new FileInputStream("src/main/resources/credentials.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
