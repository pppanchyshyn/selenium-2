package com.epam.ta10.selenium2;

import com.epam.ta10.selenium2.bl.GmailLoginPageBL;
import com.epam.ta10.selenium2.bl.GmailPasswordPageBL;
import com.epam.ta10.selenium2.bl.GmailPersonalPageBL;
import com.epam.ta10.selenium2.utils.WebDriverUtils;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestSendingEmail extends AbstractTest {

  private WebDriver driver;
  private GmailLoginPageBL gmailLoginPageBL;
  private GmailPasswordPageBL gmailPasswordPageBL;
  private GmailPersonalPageBL gmailPersonalPageBL;

  @BeforeClass
  public void setUp() {
    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    driver = WebDriverUtils.getDriver();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.manage().window().maximize();
  }

  @BeforeMethod
  public void initializeBLObjects() {
    gmailLoginPageBL = new GmailLoginPageBL();
    gmailPasswordPageBL = new GmailPasswordPageBL();
    gmailPersonalPageBL = new GmailPersonalPageBL();
  }

  @Test
  public void checkUserAbilityToSendEmail() {
    driver.get(
        "https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
    gmailLoginPageBL.enterLogin(properties.getProperty("login"));
    gmailPasswordPageBL.enterPassword(properties.getProperty("password"));
    gmailPersonalPageBL.moveToSentMessages();
    int countSentMessagesBeforeCreatingNewOne = gmailPersonalPageBL.getSentMessagesCount();
    gmailPersonalPageBL.sendMessage(Const.MESSAGE);
    gmailPersonalPageBL.moveToSentMessages();
    int countSentMessagesAfterCreatingNewOne = gmailPersonalPageBL.getSentMessagesCount();
    Assert.assertEquals(countSentMessagesAfterCreatingNewOne,
        countSentMessagesBeforeCreatingNewOne + 1,
        "The number of letters before and after sending does not match");
  }

  @AfterMethod
  public void cleanUp() {
    driver.manage().deleteAllCookies();
  }

  @AfterClass
  public void tearDown() {
    driver.quit();
  }
}
